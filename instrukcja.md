### Compilation in Docker
```
docker run -it ubuntu:rolling /bin/bash

apt update

apt install -y git

git clone https://gitlab.com/Remmina/Remmina.git
cd Remmina

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

#Z#
apt build-dep -y remmina

cmake  -DWITH_FREERDP=OFF  .

make -j8
```

### QTCreator Includes
```
/usr/include/freerdp2
/usr/include/glib-2.0/
plugins/
/usr/include/gtk-3.0/
/usr/include/libsecret-1/

```
